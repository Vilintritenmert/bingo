<?php

namespace BingoTest;

use Bingo\CardFactory;
use Bingo\CardsChecker;
use Bingo\Bound;
use Bingo\Range;
use PHPUnit\Framework\TestCase;

class CardsCheckerTest extends TestCase
{

    private const VALID_LOWER_BOUND = 1;

    private const VALID_UPPER_BOUND = 75;

    private const NOT_VALID_UPPER_BOUND = 25;

    /**
     *
     */
    public function testCardIsWin(): void
    {
        $range = new Range(new Bound(static::VALID_LOWER_BOUND, static::VALID_UPPER_BOUND));

        $card = (new CardFactory)->makeAmericanCard();
        $isWin = (new CardsChecker($range->getIterator()))->isCardWin($card);

        $this->assertTrue($isWin);
    }

    public function testCardIsNotWin(): void
    {
        $range = new Range(new Bound(static::VALID_LOWER_BOUND, static::NOT_VALID_UPPER_BOUND));

        $card = (new CardFactory)->makeAmericanCard();
        $isWin = (new CardsChecker($range->getIterator()))->isCardWin($card);

        $this->assertFalse($isWin);
    }

}