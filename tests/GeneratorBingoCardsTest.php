<?php

namespace BingoTest;

use Bingo\CardFactory;
use PHPUnit\Framework\TestCase;

class GeneratorBingoCardsTest extends TestCase {

    public function testCardCreation(): void
    {
        $card = (new CardFactory())->makeAmericanCard();

        foreach($card->getColumns() as $columnNumber => $column)
        {
            foreach($column as $valueNumber => $value) {

                if ((2 === $columnNumber) && (2 === $valueNumber)) {
                    $this->assertTrue(is_null($value));
                } else {
                    $this->assertTrue(is_numeric($value));
                }
            }
        }

    }

}