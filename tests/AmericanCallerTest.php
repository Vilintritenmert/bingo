<?php
declare(strict_types=1);

namespace BingoTest;

use Bingo\AmericanCaller;
use Bingo\Bound;
use Bingo\OutOfRangeException;
use Bingo\Range;
use PHPUnit\Framework\TestCase;

class AmericanCallerTest extends TestCase {

    private const LOWER_BOUND = 1;

    private const UPPER_BOUND = 75;

    private function getBound(): Bound
    {
        return new Bound(static::LOWER_BOUND, static::UPPER_BOUND);
    }

    private function getRange(Bound $bound): Range
    {
        return new Range($bound);
    }

    public function testCallNumber(): void
    {
        $bingoCaller  = new AmericanCaller();
        $validRange =$this->getRange($this->getBound());

        foreach($validRange->getIterator() as $key => $index) {
            $generatedNumber = $bingoCaller->callNumber();
            $inBound = $this->getBound()->numberIsInBound($generatedNumber);

            $this->assertTrue($inBound);
        }
    }

    /**
     * @throws OutOfRangeException
     */
    public function testOutOfBounds(): void
    {
        $bingoCaller  = new AmericanCaller();
        $validRange =$this->getRange($this->getBound());

        foreach($validRange->getIterator() as $key => $index) {
            $bingoCaller->callNumber();
        }

        $this->expectException(OutOfRangeException::class);
        $bingoCaller->callNumber();

    }

    /**
     * @throws OutOfRangeException
     */
    public function testRestartCaller(): void
    {
        $americanBingoCaller = new AmericanCaller();

        $validRange = $this->getRange($this->getBound());
        foreach ($validRange->getIterator() as $index) {
            $americanBingoCaller->callNumber();
        }

        $americanBingoCaller->restart();

        $validRange = $this->getRange($this->getBound());
        foreach ($validRange->getIterator() as $index) {
            $americanBingoCaller->callNumber();
        }

        $this->assertTrue(true);
    }

}
