<?php
declare(strict_types=1);

namespace BingoTest;

use Bingo\Bound;
use Bingo\ShuffledRange;
use PHPUnit\Framework\TestCase;

class NumberShufflerTest extends TestCase
{

    private CONST LOWER_BOUND = 1;

    private CONST UPPER_BOUND = 100;

    private function getBound(): Bound
    {
        return new Bound(static::LOWER_BOUND, static::UPPER_BOUND);
    }

    private function getShuffledRange(Bound $bound): ShuffledRange
    {
        return new ShuffledRange($bound);
    }

    public function testBounds(): void
    {
        $bound = $this->getBound();
        $numberShuffler = $this->getShuffledRange($bound);
        foreach ($numberShuffler->getIterator() as $number) {
            $isInbound = $bound->numberIsInBound($number);

            $this->assertTrue($isInbound);
        }
    }

}