<?php
declare(strict_types=1);

namespace Bingo;

class ShuffledRange {

    private $range;

    private $shuffled;

    private $current;

    public function __construct(Bound $bound)
    {
        $this->range = new Range($bound);
        $this->restart();
    }

    public function restart(): void
    {
        $range  = $this->range->getIterator();
        shuffle($range);

        $this->shuffled = $range;
        $this->current = -1;
    }

    public function getIterator(): array
    {
        return $this->shuffled;
    }

    public function getBound(): Bound
    {
        return $this->range->getBound();
    }

    public function getNumber(): int
    {
        $this->current++;
        $nextElementNotExists = !array_key_exists($this->current, $this->shuffled);
        if ($nextElementNotExists) {
            throw new OutOfRangeException();
        }
        return $this->shuffled[$this->current];

    }

}