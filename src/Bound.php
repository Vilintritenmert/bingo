<?php
declare(strict_types=1);

namespace Bingo;

class Bound {

    /**
     * @var int
     */
    private $lower;

    /**
     * @var int
     */
    private $upper;

    public function __construct(
        int $lower, int $upper
    ) {
        $this->lower = $lower;
        $this->upper = $upper;
    }

    /**
     * @return int
     */
    public function getLower(): int
    {
        return $this->lower;
    }

    /**
     * @return int
     */
    public function getUpper(): int
    {
        return $this->upper;
    }

    /**
     * @param int $number
     *
     * @return bool
     */
    public function numberIsInBound(int $number): bool
    {
        $isMoreOrEqualThenLower = ($this->lower <= $number);
        $isLessOrEqualThenNumber = ($this->upper >= $number);

        return ($isLessOrEqualThenNumber && $isMoreOrEqualThenLower);
    }
}