<?php
declare(strict_types=1);

namespace Bingo;

class Card {

    private $columns;

    public function __construct(
        array $columns
    ) {
        $this->columns = $columns;
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

}