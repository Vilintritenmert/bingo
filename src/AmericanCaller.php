<?php
declare(strict_types=1);

namespace Bingo;

class AmericanCaller extends AbstractCaller {

    protected function getLowerBound(): int
    {
        return 1;
    }

    protected function getUpperBound(): int
    {
        return 75;
    }
}