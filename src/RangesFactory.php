<?php
declare(strict_types=1);

namespace Bingo;

class RangesFactory  {

    public function makeShuffledRange(
        int $lowerBound,
        int $upperBound
    ): ShuffledRange {
        $bound = new Bound($lowerBound, $upperBound);

        return new ShuffledRange($bound);
    }
}