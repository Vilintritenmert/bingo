<?php
declare(strict_types=1);

namespace Bingo;

class CardFactory {


    /**
     * @var RangesFactory
     */
    private $rangesFactory;

    public function __construct()
    {
        $this->rangesFactory = new RangesFactory();
    }

    private function makeColumn(
        ShuffledRange $ranges, array $columnPattern
    ): array {
        $column = [];

        foreach($columnPattern as $row) {
            $value = null;
            if ($row) {
                $value = $ranges->getNumber();
            }
            $column[] = $value;
        }

        return $column;
    }

    public function makeAmericanCard(): Card
    {
        $bColumn = $this->makeColumn(
            $this->rangesFactory->makeShuffledRange(1, 15),
            [true, true, true, true, true]
        ) ;

        $iColumn = $this->makeColumn(
            $this->rangesFactory->makeShuffledRange(16, 30),
            [true, true, true, true, true]
        );

        $nColumn = $this->makeColumn(
            $this->rangesFactory->makeShuffledRange(31, 45),
            [true, true, false, true, true]
        );

        $gColumn = $this->makeColumn(
            $this->rangesFactory->makeShuffledRange(46, 60),
            [true, true, true, true, true]
        );
        $oColumn = $this->makeColumn(
            $this->rangesFactory->makeShuffledRange(61, 75),
            [true, true, true, true, true]
        );

        return new Card([
            $bColumn, $iColumn, $nColumn, $gColumn, $oColumn
        ]);
    }
}