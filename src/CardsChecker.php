<?php
declare(strict_types=1);

namespace Bingo;

class CardsChecker {

    /**
     * @var array
     */
    private $numberHistory;

    public function __construct(
        array $numberHistory
    ) {
        $this->numberHistory = $numberHistory;
    }

    public function isCardWin(Card $card): bool
    {
        $numbers = $card->getColumns();
        $mergedColumns = array_filter(array_merge(...$numbers));

        $historyNumber = $this->numberHistory;
        $countNumberExists = count(array_filter($mergedColumns, function(int $number) use ($historyNumber) {
            return in_array($number, $historyNumber, true);
        }));
        $totalCount = count($mergedColumns);

        return ($countNumberExists === $totalCount);
    }

}