<?php
declare(strict_types=1);

namespace Bingo;

class Range {

    /**
     * @var Bound
     */
    private $bound;

    public function __construct(
        Bound $bound
    ) {
        $this->bound = $bound;
    }

    /**
     * @return Bound
     */
    public function getBound(): Bound
    {
        return $this->bound;
    }

    public function getIterator(): array
    {
        return range($this->bound->getLower(), $this->bound->getUpper());
    }

}