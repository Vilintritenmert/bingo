<?php

namespace Bingo;

abstract class AbstractCaller
{

    protected $shuffled;

    /**
     * @var Bound
     */
    protected $bound;

    private $current;

    public function __construct()
    {
        $this->bound = new Bound($this->getLowerBound(), $this->getUpperBound());
        $this->restart();
    }

    abstract protected function getLowerBound(): int;

    abstract protected function getUpperBound(): int;

    /**
     * @return mixed
     * @throws OutOfRangeException
     */
    public function callNumber()
    {
        return $this->shuffled->getNumber();
    }

    public function restart(): void
    {
        $this->current = -1;
        $this->shuffled = (new RangesFactory())->makeShuffledRange(
            $this->getLowerBound(),
            $this->getUpperBound()
        );
    }

}